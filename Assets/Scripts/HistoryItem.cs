﻿using System;
using SQLite4Unity3d;

[Serializable]
public class HistoryItem
{
    
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    public string Message { get; set; }
    public string Result { get; set; }

    public HistoryItem(string message, string result)
    {
        Message = message;
        Result = result;
    }

    public HistoryItem()
    {
        
    }
    
}