﻿using UnityEngine;

[CreateAssetMenu]
public class LocalizationList : ScriptableObject
{
    public LocalizationItem[] items;

    
}