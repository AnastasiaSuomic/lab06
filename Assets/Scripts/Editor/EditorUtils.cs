﻿using System;
using UnityEditor;
using UnityEngine;

public static class EditorUtils
{
    [MenuItem("Utils/OpenPersistentFolder")]
    private static void OpenPersistentFolder()
    {
        Application.OpenURL($"file://{Application.persistentDataPath}");
    }
}