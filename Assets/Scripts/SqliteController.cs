﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using System.IO;
using SQLite4Unity3d;

public class SqliteController : MonoBehaviour
{
    private string databaseName = "database.db";
    private SQLiteConnection _connection;

    //реализация паттерна синглтон
    public static SqliteController Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    private IEnumerator Start()
    {
        yield return CreateDatabaseIfNeed();
        GetConnection();
        CreateTablesIfNeed();
    }

    private void OnDestroy()
    {
        if (_connection != null)
        {
            _connection.Close();
            _connection = null;
        }
    }

    public void InsertItem(HistoryItem item)
    {
        GetConnection().Insert(item);
    }

    public void ClearItems()
    {
        GetConnection().DeleteAll<HistoryItem>();
    }
    
    private IEnumerator CreateDatabaseIfNeed()
    {
        // check if file exists in Application.persistentDataPath
        var filepath = $"{Application.persistentDataPath}/{databaseName}";

        if (!File.Exists(filepath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID && !UNITY_EDITOR
            var loadDb =
                new WWW("jar:file://" + Application.dataPath + "!/assets/" +
                        databaseName); // this is the path to your StreamingAssets in android
            while (!loadDb.isDone)
            {
                yield return null;
            }

            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_EDITOR
            File.Copy(Path.Combine(Application.dataPath, "StreamingAssets", databaseName), filepath);
#endif

            Debug.Log("Database written");
        }
        yield break;
    }

    private void CreateTablesIfNeed()
    {
        var dbcon = GetConnection();
        dbcon.CreateTable<HistoryItem>();
    }

    private string GetDbPath()
    {
#if UNITY_EDITOR
        return $"Assets/StreamingAssets/{databaseName}";
#elif UNITY_ANDROID
        return $"{Application.persistentDataPath}/{databaseName}";
#else 
    return "";
#endif
    }

    public IEnumerable<HistoryItem> GetHistoryItems()
    {
        return GetConnection().Table<HistoryItem>();
    }

    private SQLiteConnection GetConnection()
    {
        return _connection ??
               (_connection = new SQLiteConnection(GetDbPath(), SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create));
    }
    
}
