﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Toggle = UnityEngine.UI.Toggle;

public class HistoryViewer : MonoBehaviour
{
    [SerializeField] private HistoryItemVisual itemPrefab;
    [SerializeField] private Transform itemsContainer;
    [SerializeField] private Button buttonCloseHistory;
    [SerializeField] private Button buttonClearHistory;
    [SerializeField] private Toggle toggleLastFive;
    
    private List<HistoryItemVisual> _spawnedItems;

    private bool SavedLastFive
    {
        get => PlayerPrefs.GetInt("historyLastFive") == 1 ? true : false;
        set
        {
            PlayerPrefs.SetInt("historyLastFive", value ? 1 : 0);
            PlayerPrefs.Save();
        }
    }

    private void Awake()
    {
        _spawnedItems = new List<HistoryItemVisual>();
        buttonCloseHistory.onClick.AddListener(ButtonCloseHistoryOnClick);
        buttonClearHistory.onClick.AddListener(ButtonClearHistoryOnClick);
        toggleLastFive.isOn = SavedLastFive;
        toggleLastFive.onValueChanged.AddListener(ToggleLastFiveOnChanged);
    }

    private void ToggleLastFiveOnChanged(bool isOn)
    {
        SavedLastFive = isOn;
        SpawnItems();
    }

    private void ButtonCloseHistoryOnClick()
    {
        gameObject.SetActive(false);
    }
    
    private void ButtonClearHistoryOnClick()
    {
        HistoryManager.Instance.Clear();
        DestroyItems();
    }

    private void DestroyItems()
    {
        foreach (var item in _spawnedItems)
        {
            Destroy(item.gameObject);
        }
        _spawnedItems.Clear();
    }

    private void SpawnItems()
    {
        DestroyItems();
        
        var count = HistoryManager.Instance.Items.Count;
        var startIndex = 0;
        
        if (toggleLastFive.isOn)
        {
            startIndex = Math.Max(0, count - 5);
        }
        
        for (var index = startIndex; index < count; index++)
        {
            var item = HistoryManager.Instance.Items[index];
            var panel = Instantiate(itemPrefab, itemsContainer);
            panel.LoadItem(item);
            _spawnedItems.Add(panel);
        }
    }

    private void OnEnable()
    {
        SpawnItems();
    }
}